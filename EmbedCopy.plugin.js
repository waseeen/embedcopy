/**
 * @name EmbedCopy
 * @author waseeeen
 * @authorId 422000277563113472
 * @version 1.1.4ex
 * @description Embed copyer
 * @source https://gitlab.com/waseeen/embedcopy
 * @upadteUrl https://gitlab.com/waseeen/embedcopy/-/raw/master/EmbedCopy.plugin.js
 */

 module.exports = (_ => {
	const changeLog = {
		
	};

	return !window.BDFDB_Global || (!window.BDFDB_Global.loaded && !window.BDFDB_Global.started) ? class {
		constructor (meta) {for (let key in meta) this[key] = meta[key];}
		getName () {return this.name;}
		getAuthor () {return this.author;}
		getVersion () {return this.version;}
		getDescription () {return `The Library Plugin needed for ${this.name} is missing. Open the Plugin Settings to download it. \n\n${this.description}`;}
		
		downloadLibrary () {
			require("request").get("https://mwittrien.github.io/BetterDiscordAddons/Library/0BDFDB.plugin.js", (e, r, b) => {
				if (!e && b && r.statusCode == 200) require("fs").writeFile(require("path").join(BdApi.Plugins.folder, "0BDFDB.plugin.js"), b, _ => BdApi.showToast("Finished downloading BDFDB Library", {type: "success"}));
				else BdApi.alert("Error", "Could not download BDFDB Library Plugin. Try again later or download it manually from GitHub: https://mwittrien.github.io/downloader/?library");
			});
		}
		
		load () {
			if (!window.BDFDB_Global || !Array.isArray(window.BDFDB_Global.pluginQueue)) window.BDFDB_Global = Object.assign({}, window.BDFDB_Global, {pluginQueue: []});
			if (!window.BDFDB_Global.downloadModal) {
				window.BDFDB_Global.downloadModal = true;
				BdApi.showConfirmationModal("Library Missing", `The Library Plugin needed for ${this.name} is missing. Please click "Download Now" to install it.`, {
					confirmText: "Download Now",
					cancelText: "Cancel",
					onCancel: _ => {delete window.BDFDB_Global.downloadModal;},
					onConfirm: _ => {
						delete window.BDFDB_Global.downloadModal;
						this.downloadLibrary();
					}
				});
			}
			if (!window.BDFDB_Global.pluginQueue.includes(this.name)) window.BDFDB_Global.pluginQueue.push(this.name);
		}
		start () {this.load();}
		stop () {}
		getSettingsPanel () {
			let template = document.createElement("template");
			template.innerHTML = `<div style="color: var(--header-primary); font-size: 16px; font-weight: 300; white-space: pre; line-height: 22px;">The Library Plugin needed for ${this.name} is missing.\nPlease click <a style="font-weight: 500;">Download Now</a> to install it.</div>`;
			template.content.firstElementChild.querySelector("a").addEventListener("click", this.downloadLibrary);
			return template.content.firstElementChild;
		}
	} : (([Plugin, BDFDB]) => {
		return class EmbedCopy extends Plugin {
			onLoad () {
				this.modulePatches = {
					after: [
						"MessageActionsContextMenu",
						"MessageToolbar"
					]
				};
			}
			
			onStart () {}
			
			onStop () {}

			renameKey(object, key, newKey) {
				const clone = (obj) => Object.assign({}, obj);
				const clonedObj = clone(object);
				const targetKey = clonedObj[key];
				delete clonedObj[key];
				clonedObj[newKey] = targetKey;
				return clonedObj;
	
			};

			hsl_ToHex(hsl) {
				hsl = hsl.match(/^hsla?\(\s?(\d+)(?:deg)?,?\s(\d+)%,?\s(\d+)%,?\s?(?:\/\s?\d+%|\s+[\d+]?\.?\d+)?\)$/i);
				if(!hsl){
				  return null;
				}
				let h = hsl[1];
				let s = hsl[2];
				let l = hsl[3];
				h /= 360;
				s /= 100;
				l /= 100;
				let r, g, b;
				if (s === 0) {
				  r = g = b = l;
				} else {
				  const hue2rgb = function(p, q, t) {
					if (t < 0) t += 1;
					if (t > 1) t -= 1;
					if (t < 1 / 6) return p + (q - p) * 6 * t;
					if (t < 1 / 2) return q;
					if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
					return p;
				  };
				  const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
				  const p = 2 * l - q;
				  r = hue2rgb(p, q, h + 1 / 3);
				  g = hue2rgb(p, q, h);
				  b = hue2rgb(p, q, h - 1 / 3);
				}
				const toHex = function(x) {
				  const hex = Math.round(x * 255).toString(16);
				  return hex.length === 1 ? '0' + hex : hex;
				};
				return '#'+toHex(r)+toHex(g)+toHex(b);
			  }

			HSLToHex(hsl) { 
				let sep = hsl.indexOf(",") > -1 ? "," : " ";
				hsl = hsl.substr(4).split(")")[0].split(sep);
			  
				let h = hsl[0],
					s = hsl[1].substr(0,hsl[1].length - 1) / 100,
					l = hsl[2].substr(0,hsl[2].length - 1) / 100;
					  
				// Strip label and convert to degrees (if necessary)
				if (h.indexOf("deg") > -1)
				  h = h.substr(0,h.length - 3);
				else if (h.indexOf("rad") > -1)
				  h = Math.round(h.substr(0,h.length - 3) * (180 / Math.PI));
				else if (h.indexOf("turn") > -1)
				  h = Math.round(h.substr(0,h.length - 4) * 360);
				if (h >= 360)
				  h %= 360;
			  }

			HSLToDec(h,s,l) {
				s /= 100;
				l /= 100;
			  
				let c = (1 - Math.abs(2 * l - 1)) * s,
					x = c * (1 - Math.abs((h / 60) % 2 - 1)),
					m = l - c/2,
					r = 0,
					g = 0, 
					b = 0; 
			  
				if (0 <= h && h < 60) {
				  r = c; g = x; b = 0;
				} else if (60 <= h && h < 120) {
				  r = x; g = c; b = 0;
				} else if (120 <= h && h < 180) {
				  r = 0; g = c; b = x;
				} else if (180 <= h && h < 240) {
				  r = 0; g = x; b = c;
				} else if (240 <= h && h < 300) {
				  r = x; g = 0; b = c;
				} else if (300 <= h && h < 360) {
				  r = c; g = 0; b = x;
				}
				// Having obtained RGB, convert channels to hex
				r = Math.round((r + m) * 255).toString(16);
				g = Math.round((g + m) * 255).toString(16);
				b = Math.round((b + m) * 255).toString(16);
				
				// Prepend 0s, if necessary
				if (r.length == 1)
				  r = "0" + r;
				if (g.length == 1)
				  g = "0" + g;
				if (b.length == 1)
				  b = "0" + b;
				  
				let hex = r + g + b
				return parseInt(hex, 16);
			  }
	

			onMessageContextMenu (e) {
				if (e.instance.props.message) {
					let content = e.instance.props.message.content;
					let messageString = [e.instance.props.message.content, BDFDB.ArrayUtils.is(e.instance.props.message.attachments) && e.instance.props.message.attachments.map(n => n.url)].flat(10).filter(n => n).join("\n");
					let selectedText = document.getSelection().toString().trim();
					if (selectedText) messageString = BDFDB.StringUtils.extractSelection(messageString, selectedText);
					let embed = BDFDB.DOMUtils.getParent(BDFDB.dotCN.embedwrapper, e.instance.props.target);
					let embedData = e.instance.props.message.embeds[embed ? Array.from(embed.parentElement.querySelectorAll(BDFDB.dotCN.embedwrapper)).indexOf(embed) : -1];

					//embed magic

					let embedD = embedData;
                    embedD = this.renameKey(embedD, "rawDescription", "description");
                    embedD = this.renameKey(embedD, "rawTitle", "title");
					delete embedD.type;
					delete embedD.url;
					delete embedD.referenceId;
					delete embedD.id;
					if (embedD.author !== undefined) {
						delete embedD.author.iconProxyURL
						embedD.author = this.renameKey(embedD.author, "iconURL", "icon_url")
					}

					if (embedD.color !== undefined) {
						let regExp = /\d{1,}/g;
						let hsl = embedD.color.split(" ");
						let hue = hsl[0].match(regExp)[0];
						let saturation = hsl[4].match(regExp)[0];
						let lightness = hsl[5].match(regExp)[0];
						// let hue = 1;
						// let saturation = 1;
						// let lightness = 1;
						let hex = this.HSLToDec(hue,saturation,lightness);
						embedD.color = hex;
					}

                    if (embedD.footer !== undefined) {
						delete embedD.footer.iconProxyURL
						embedD.footer = this.renameKey(embedD.footer, "iconURL", "icon_url")
                     }
					 if (embedD.thumbnail !== undefined) {
						delete embedD.thumbnail.proxyURL
						delete embedD.thumbnail.width
						delete embedD.thumbnail.height
					}
					if (embedD.image !== undefined) {
						delete embedD.image.proxyURL
						delete embedD.image.width
						delete embedD.image.height
					}

					if (embedD.fields !== undefined) {
						let fields = embedD.fields.map((e) => {
							let obj = {
								name: e.rawName,
								value: e.rawValue,
								inline: e.inline
							}
							return obj
						})
						embedD.fields = fields;
					}


					//embed magic ends
					let embedString = embedData && [embedData.rawTitle, embedData.rawDescription, BDFDB.ArrayUtils.is(embedData.fields) && embedData.fields.map(n => [n.rawName, n.rawValue]), BDFDB.ObjectUtils.is(embedData.image) && embedData.image.url, BDFDB.ObjectUtils.is(embedData.footer) && embedData.footer.text].flat(10).filter(n => n).join("\n");
					if (selectedText) embedString = BDFDB.StringUtils.extractSelection(embedString, selectedText);
					let hint = BDFDB.BDUtils.isPluginEnabled("MessageUtilities") ? BDFDB.BDUtils.getPlugin("MessageUtilities").getActiveShortcutString("Copy_Raw") : null;
					let entries = [
						embedData && BDFDB.ContextMenuUtils.createItem(BDFDB.LibraryComponents.MenuItems.MenuItem, {
							label: BDFDB.LanguageUtils.LanguageStrings.COPY_TEXT + " (Embed JSON fromatted)",
							type: "Embed JSON formatted",
							id: BDFDB.ContextMenuUtils.createItemId(this.name, "copy-embed-json-fromatted"),
							icon: _ => BDFDB.ReactUtils.createElement(BDFDB.LibraryComponents.MenuItems.MenuIcon, {
								icon: BDFDB.LibraryComponents.SvgIcon.Names.RAW_TEXT
							}),
							action: _ => BDFDB.LibraryModules.WindowUtils.copy(JSON.stringify(embedD))
						})
					].filter(n => n);
					if (entries.length) {
						let [children, index] = BDFDB.ContextMenuUtils.findItem(e.returnvalue, {id: "copy-link"});
						children.splice(index > -1 ? index + 1 : children.length, 0, entries.length > 1 ? BDFDB.ContextMenuUtils.createItem(BDFDB.LibraryComponents.MenuItems.MenuItem, {
							label: BDFDB.LanguageUtils.LanguageStrings.COPY_TEXT,
							id: BDFDB.ContextMenuUtils.createItemId(this.name, "copy-raw-submenu"),
							children: entries.map(n => {
								n.props.label = n.props.type;
								delete n.props.type;
								delete n.props.icon;
								return n;
							})
						}) : entries);
					}
				}
			}

			processMessageActionsContextMenu (e) {
				if (e.instance.props.message && e.instance.props.message.content) {
					let [children, index] = BDFDB.ContextMenuUtils.findItem(e.returnvalue, {id: "copy-link"});
					children.splice(index + 1, 0, BDFDB.ContextMenuUtils.createItem(BDFDB.LibraryComponents.MenuItems.MenuItem, {
						label: BDFDB.LanguageUtils.LanguageStrings.COPY_TEXT + " (Raw)",
						id: BDFDB.ContextMenuUtils.createItemId(this.name, "copy-message-raw"),
						icon: _ => BDFDB.ReactUtils.createElement(BDFDB.LibraryComponents.MenuItems.MenuIcon, {
							icon: BDFDB.LibraryComponents.SvgIcon.Names.RAW_TEXT
						}),
						action: _ => BDFDB.LibraryModules.WindowUtils.copy(e.instance.props.message.content)
					}));
				}
			}
		
			processMessageToolbar (e) {
				if (e.instance.props.expanded && e.instance.props.message && e.instance.props.channel) {
					e.returnvalue.props.children.unshift(BDFDB.ReactUtils.createElement(BDFDB.LibraryComponents.TooltipContainer, {
						key: "copy-message-raw",
						text: BDFDB.LanguageUtils.LanguageStrings.COPY_TEXT + " (Raw)",
						children: BDFDB.ReactUtils.createElement(BDFDB.LibraryComponents.Clickable, {
							className: BDFDB.disCN.messagetoolbarbutton,
							onClick: _ => {
								BDFDB.LibraryModules.WindowUtils.copy(e.instance.props.message.content);
							},
							children: BDFDB.ReactUtils.createElement(BDFDB.LibraryComponents.SvgIcon, {
								className: BDFDB.disCN.messagetoolbaricon,
								name: BDFDB.LibraryComponents.SvgIcon.Names.RAW_TEXT
							})
						})
					}));
				}
			}
		};
	})(window.BDFDB_Global.PluginUtils.buildPlugin(changeLog));
})();
